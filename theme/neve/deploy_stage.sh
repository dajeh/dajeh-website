#!/bin/sh

SCRIPTPATH=$( cd $(dirname $0) ; pwd -P )

# Exclude file
EXCLUDE_FILE="${SCRIPTPATH}/rsync_exclude.txt"

# Directory to sync with stage
SYNC_DIR="${SCRIPTPATH}"

# Host
HOST=kalaflax-web-stage

# User on host
USER=root

# Path on host
HOST_PATH=/var/www/vhosts/stage.dajeh.de/httpdocs/wp-content/themes/neve/

#
# Deploy script for staging server
#
# -a             = archive mode
# -v             = verbose
# -z             = compress
# --delete       = delete unknown files from destination during sync
# --stats        = file transfer stats
# --exclude-from = exclusion list
#
# --dry-run      = run simulation
#
rsync -rlDvz \
	--delete \
	--stats \
	-og --chown=kalaflax:psacln \
	--exclude-from ${SCRIPTPATH}/rsync_exclude.txt \
	${SCRIPTPATH}/ \
	${USER}@${HOST}:${HOST_PATH}
