<?php /* Template Name: LP Dajeh */ ?>

<?php
    get_header();
?>

<div class="container">
    <div class="landing-page__section-cnt landing-page__section-cnt--first">
        <header class="landing-page__header">
            <h1 class="landing-page__ttl">
                <span class="landing-page__goal">
                    Vereinbaren Sie Ihre kostenfreie Beratung
                </span>
                <span class="landing-page__title">
                    Individuell, professionell, ausgezeichnet – Apps von Kalaflax
                </span>
            </h1>
        </header>

        <div class="landing-page__monster-cta">
            <div class="landing-page__monster-cta__btn-ct">
                <a href="#beratung-vereinbaren" title=" Jetzt kostenfrei starten*"
                   class="landing-page__monster-cta__btn">
                    App-Entwicklung starten
                </a>
            </div>
            <div class="landing-page__monster-cta__hint">
                Kostenfreie und unverbindliche Erstberatung
            </div>
        </div>

        <div class="landing-page__monster-img"></div>
    </div>
</div>



<?php
    get_footer();
?>