<?php

if (!defined('ABSPATH')) {
    exit;
}
if (!function_exists('neve_child_load_css')) :
    /**
     * Load CSS file.
     */
    function neve_child_load_css()
    {
        wp_enqueue_style('neve-child-style', trailingslashit(get_stylesheet_directory_uri()) . 'style.css', array('neve-style'), NEVE_VERSION);
    }
endif;
add_action('wp_enqueue_scripts', 'neve_child_load_css', 20);


/* Enqueue Javascript in /child-theme-s/includes/js/ */
function child_theme_s_scripts()
{
    wp_enqueue_script('child-theme-s-js', get_stylesheet_directory_uri() . '/assets/js/main.js');
}

add_action('wp_enqueue_scripts', 'child_theme_s_scripts');
