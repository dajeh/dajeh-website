/*eslint-env node*/
module.exports = function (grunt) {
  'use strict';

  require('load-grunt-tasks')(grunt);

  var sass = require('node-sass');

  /*
   * ESLint shizzle
   */
  var eslintOpts = grunt.file.readJSON('.eslintrc');
  var ignores = grunt.file.read('.eslintignore');

  eslintOpts.ignores = ignores.split('\n');

  /*
   * Grunt shizzle
   */
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    /*
     * Modernizr (custom build)
     */
    modernizr: {
      default: {
        dest: 'src/assets/js/head/modernizr.js',
        uglify: false,
        options: ['mq', 'setClasses'],
      },
    },

    /*
     * Check JS
     */
    eslint: {
      options: {
        configFile: '.eslintrc',
      },
      default: ['gruntfile.js', 'src/**/*.js'],
    },

    /*
     * Concat JS
     */
    concat: {
      options: {
        separator: ';',
      },
      main: {
        nonull: true,
        dest: 'build/<%= mode %>/assets/js/main.js',
        src: [
          'src/assets/js/main/main.js',
          'src/assets/js/main/**/*.js',
          'src/assets/js/main/*.js',
        ],
      },
      //   head: {
      //     nonull: true,
      //     dest: 'build/<%= mode %>/assets/js/head.js',
      //     src: [
      //       'src/assets/js/head/modernizr.js',
      //       'node_modules/svg4everybody/dist/svg4everybody.js',
      //       'src/assets/js/head/*.js',
      //     ],
      //   },
    },

    /*
     * Compile to ES5
     */
    babel: {
      options: {
        sourceMap: true,
        presets: ['@babel/preset-env'],
      },
      dist: {
        files: {
          'build/<%= mode %>/assets/js/main.js':
            'build/<%= mode %>/assets/js/main.js',
        },
      },
    },

    /*
     * Minify JS
     */
    uglify: {
      default: {
        files: [
          {
            expand: true,
            cwd: 'build/<%= mode %>/assets/js/',
            src: '**/*.js',
            dest: 'build/<%= mode %>/assets/js/',
          },
        ],
        banner:
          '/*!\n * (c) <%= pkg.customer %> <%= grunt.template.today("yyyy") %> \n */\n',
      },
    },

    /*
     * Check styles
     */
    /*  scsslint: {
              options: {
                  colorizeOutput: true,
                  force: true
              },
              default: [
                  'src/assets/styles/!**!/!*.scss'
              ]
          },*/

    /*
     * Compile styles
     */
    sass: {
      options: {
        implementation: sass,
        sourcemap: 'none',
        style: 'expanded',
        includePaths: ['node_modules'],
      },
      default: {
        files: {
          'build/<%= mode %>/style.css': 'src/assets/styles/style.scss',
        },
      },
    },

    /*
     * Autoprefix styles
     */
    autoprefixer: {
      options: {
        browsers: ['last 3 version', 'ie 9', 'Firefox ESR', 'Opera 12.1'],
      },
      default: {
        src: 'build/<%= mode %>/style.css',
        dest: 'build/<%= mode %>/style.css',
      },
    },

    /*
     * Minimize css
     */
    cssmin: {
      default: {
        options: {
          keepSpecialComments: 1,
          compatibility: 'ie8',
          rebase: false,
        },
        files: {
          'build/<%= mode %>/assets/style.css': [
            'build/<%= mode %>/assets/style.css',
          ],
          'build/<%= mode %>/assets/images/symbols.css': [
            'build/<%= mode %>/assets/images/symbols.css',
          ],
        },
      },
    },

    /*
     * Minimize bit-images
     */
    imagemin: {
      options: {
        cache: false,
        svgoPlugins: [
          {
            removeViewBox: false,
          },
          {
            removeUselessStrokeAndFill: false,
          },
        ],
      },
      default: {
        files: [
          {
            expand: true,
            cwd: 'src/assets/images/',
            src: ['**/*.{png,jpg,gif,svg}'],
            dest: 'build/<%= mode %>/assets/images/',
          },
        ],
      },
    },

    /*
     * SVG sprite
     */
    svg_sprite: {
      complex: {
        cwd: 'src/assets/svg',
        expand: true,
        src: ['**/*.svg'],
        dest: 'build/<%= mode %>/assets',
        options: {
          shape: {
            dimension: {
              attributes: true,
            },
          },
          mode: {
            symbol: {
              dest: 'images',
              sprite: 'symbols.svg',
              dimensions: false,
              render: {
                css: {
                  dest: 'symbols.css',
                },
              },
            },
          },
        },
      },
    },

    /*
     * SVG minimization
     */
    svgmin: {
      options: {
        plugins: [
          {
            removeViewBox: false,
          },
          {
            removeUselessStrokeAndFill: false,
          },
        ],
      },
      default: {
        // Target
        files: [
          {
            // Dictionary of files
            expand: true, // Enable dynamic expansion.
            cwd: 'build/<%= mode %>/assets/images/', // Src matches are relative to this path.
            src: ['**/*.svg'], // Actual pattern(s) to match.
            dest: 'build/<%= mode %>/assets/images/', // Destination path prefix.
            ext: '.svg', // Dest filepaths will have this extension.
          },
        ],
      },
    },

    /*
     * Copy files
     */
    copy: {
      common: {
        files: [
          {
            expand: true,
            dot: true,
            cwd: 'src',
            src: ['*', 'assets/fonts/**', 'inc/**', '**/*.php', '*'],
            dest: 'build/<%= mode %>',
          },
        ],
      },
      dev: {
        files: [
          {
            // Images, unoptimized
            expand: true,
            cwd: 'src/assets/images/',
            src: ['**/*.{png,jpg,gif,svg}'],
            dest: 'build/<%= mode %>/assets/images/',
          },
        ],
      },
      videos: {
        files: [
          {
            // Videos
            expand: true,
            cwd: 'src/assets/videos/',
            src: ['**/*.{mp4,webm,ogg}'],
            dest: 'build/<%= mode %>/assets/videos/',
          },
        ],
      },
    },

    /*
     * Clean
     */
    clean: {
      options: {
        force: true,
      },
      default: {
        src: ['build/<%= mode %>/*'],
      },
    },

    /*
     * Notifications
     */
    notify: {
      watch: {
        options: {
          title: 'Grunt, grunt!',
          message: 'Watch update',
        },
      },
      update: {
        options: {
          title: 'Grunt, grunt!',
          message: 'Update',
        },
      },
    },

    browserSync: {
      dev: {
        bsFiles: {
          src: ['./build/local/style.css', './build/local/assets/js/**/*.js'],
        },
        options: {
          proxy: 'http://localhost:8080',
          watchTask: true,
        },
      },
    },

    /*
     * Watch & autocompile
     */
    watch: {
      options: {
        livereload: true,
      },
      configFiles: {
        files: ['gruntfile.js', '.eslintrc', '.scss-lint.yml'],
        options: {
          reload: true,
        },
        tasks: ['default', 'notify:watch'],
      },

      sass: {
        files: ['src/assets/styles/**/*.scss'],
        tasks: [
          // 'scsslint',
          'sass',
          'autoprefixer',
          'notify:watch',
        ],
      },
      js: {
        files: ['src/assets/js/**/*.js'],
        tasks: ['newer:eslint', 'concat', 'babel', 'notify:watch'],
      },
      other: {
        files: ['src/**/*.php', 'src/assets/fonts/**', 'src/*'],
        tasks: ['newer:copy:common', 'notify:watch'],
      },
      images: {
        files: ['src/assets/images/**/*.{jpeg,jpg,png,gif}'],
        tasks: ['copy:dev', 'notify:watch'],
      },
      //   videos: {
      //     files: ['src/assets/videos/**/*.{mp4, webm, ogv}'],
      //     tasks: ['copy:videos', 'notify:watch'],
      //   },
      svg: {
        files: ['src/assets/svg/**/*.svg'],
        tasks: ['svg_sprite', 'notify:watch'],
      },
    },
  });

  // Build modes: 'local', 'stage', 'live'
  var mode = grunt.option('mode') || 'local';
  if (mode !== 'local' && mode !== 'stage' && mode !== 'live') {
    mode = 'local';
  }
  grunt.config.set('mode', mode);

  var tasks;
  if (mode === 'local') {
    tasks = [
      // Browsersync
      'copy:common',
      'copy:dev',
      //   'copy:videos',
      // Images
      'svg_sprite',
      // CSS
      'sass',
      'autoprefixer',
      // JavaScript
      'eslint',
      //   'concat',
      'babel',
    ];
  } else {
    tasks = [
      // Clean
      'clean',
      // Files
      'copy:common',
      //   'copy:videos',
      // Images
      'imagemin',
      'svg_sprite',
      // CSS
      'sass',
      'autoprefixer',
      'cssmin',
      // JavaScript
      'eslint',
      //   'concat',
      'babel',
      'uglify',
    ];
  }

  tasks.push('notify:update');

  // Register tasks
  grunt.registerTask('default', tasks);
  grunt.registerTask('dev', ['browserSync', 'watch']);
};
