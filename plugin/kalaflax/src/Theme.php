<?php

namespace Kalaflax;

/*
 * Global template variables
 */
use Kalaflax\Utils\NavigationUtils;

class Theme {

	public static $JQUERY_VERSION = '3.5.1';
	public static $JQUERY_LEGACY_VERSION = '1.12.4';
	public static $RESPOND_JS_VERSION = '1.4.2';
	public static $HTML5SHIV_VERSION = '3.7.3';

	public static $FEATURED_IMAGE_SIZES = array(
		'klflx-xs'  => 150,
		'klflx-s'   => 300,
		'klflx-m'   => 600,
		'klflx-l'   => 900,
		'klflx-xl'  => 1200,
		'klflx-xxl' => 1900
	);

	private static $INSTANCE = null;

	private $textDomain;

	private $pageId = null;

	private $wpTheme = null;

	private $supports = array();
	private $menus = array();

	private $scripts = array();

	private $pageClasses = array();

	public function __construct( $textDomain ) {

		$this->textDomain = $textDomain;

		//
		// Basic stuff
		//
		$this->supports[] = 'post-thumbnails';

		//
		// Actions
		//
		add_action( 'init', array( $this, 'init' ) );
		add_action( 'after_setup_theme', array( $this, 'setup' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueueScripts' ) );
		add_action( 'wp_footer', array( $this, 'renderScripts' ), 20 );

		// Filters
		add_filter( 'wp_title', array( $this, 'filterTitle' ), 10, 2 );
		add_filter( 'body_class', array( $this, 'filterBodyClass' ) );
		add_filter( 'script_loader_tag', array( $this, 'filterScriptLoaderTag' ), 10, 3 );

		$this->wpTheme = wp_get_theme();

		NavigationUtils::registerFilters();
	}

	public function init() {
		remove_action( 'wp_head', 'wp_generator' );
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
		remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	}

	public function setup() {
		// Set locale
		setlocale( LC_ALL, get_locale() );

		// Load translations
		load_theme_textdomain( $this->textDomain, get_template_directory() . '/languages' );

		// Add image sizes
		foreach ( self::$FEATURED_IMAGE_SIZES as $key => $value ) {
			add_image_size( $key, $value, 0, false );
		}

		/******************************************************************************\
		 * Theme support, standard settings, menus and widgets
		 * \******************************************************************************/

		add_theme_support( 'html5', array(
			'gallery',
			'caption'
		) );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );

		add_post_type_support( 'page', 'excerpt' );

		// Theme menus
		register_nav_menus( $this->menus );
	}

	/**
	 * @return string
	 */
	public function getTextDomain() {
		return $this->textDomain;
	}

	public function addMenu( $menuKey, $menuName ) {
		$this->menus[ $menuKey ] = $menuName;
	}

	public function setAsGlobal() {
		self::$INSTANCE = $this;
	}

	/**
	 * @return Theme
	 */
	public static function get() {
		return self::$INSTANCE;
	}

	public function setPageId( $pageId ) {
		$this->pageId = $pageId;
	}

	public function getPageId() {
		return $this->pageId;
	}

	public function addPageClass($class) {
		$this->pageClasses[] = $class;
	}

	// =======================================
	// 					Filters
	// =======================================

	//
	// Title
	//
	public function filterTitle( $title, $sep ) {

		if ( is_feed() ) {
			return $title;
		}

		if ( empty( $title ) ) {
			return get_bloginfo( 'name' ) . ' ' . $sep . ' ' . get_bloginfo( 'description' );
		}

		return $title . get_bloginfo( 'name' );
	}

	//
	// Body CSS classes
	//
	public function filterBodyClass( $classes ) {
		if ( ! empty( $this->pageId ) ) {
			$classes[] = 'page--' . $this->pageId;
		}

		$classes = array_merge($classes, $this->pageClasses);

		$classes[] = 'lang--' . get_locale();

		return $classes;
	}

	//
	// Handle special script tags
	//
	public function filterScriptLoaderTag( $tag, $handle, $src ) {

		// jQuery 1 CC
		if ( 0 == strcmp( $handle, 'jquery-legacy' ) ) {
			return '<!--[if lte IE 8]>' . $tag . '<![elseif]-->';
		}

		// jQuery 2 CC
		if ( 0 == strcmp( $handle, 'jquery' ) ) {
			return '<!--[if gte IE 9]><!-->' . $tag . '<!--<![endif]-->';
		}

		if ( strpos( $tag, '#defer' ) !== false ) {
			$tag = str_replace( '#defer', '', $tag );
			$tag = str_replace( '<script ', '<script defer ', $tag );
		}

		return $tag;
	}


	/*
	 * Shortcodes
	 */
	public function addShortcodes() {
		// Nothing yet
	}

	/******************************************************************************\
	 * Scripts and Styles
	 * \******************************************************************************/

	/**
	 * Enqueue theme scripts
	 * @return void
	 */
	public function enqueueScripts() {
		// Remove standard style.css
		wp_dequeue_style( 'generate-style' );

		//
		// Header elements
		//

		// Styles
		wp_enqueue_style( 'theme-styles', get_template_directory_uri() . '/assets/style.css', null, $this->wpTheme->get( 'Version' ) );

		// Respond js
		wp_enqueue_script( 'respond.js', get_template_directory_uri() . '/vendor/respondjs/respond.min.js', null, self::$RESPOND_JS_VERSION );
		wp_script_add_data( 'respond.js', 'conditional', 'lt IE 9' );

		// HTML5 shiv
		wp_enqueue_script( 'html5shiv', get_template_directory_uri() . '/vendor/html5shiv/html5shiv' . ( WP_DEBUG ? '' : '.min' ) . '.js', $deps = null, self::$HTML5SHIV_VERSION );
		wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );

		//
		// Footer elements
		//

		// Footer elements
		// jQuery 1.x (legacy for IE < 9)
		wp_register_script( 'jquery-legacy', '//ajax.googleapis.com/ajax/libs/jquery/' . self::$JQUERY_LEGACY_VERSION . '/jquery' . ( WP_DEBUG ? '' : '.min' ) . '.js', $deps = null, $ver = null, true );
		wp_enqueue_script( 'jquery-legacy' );

		// jQuery 2.x
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/' . self::$JQUERY_VERSION . '/jquery' . ( WP_DEBUG ? '' : '.min' ) . '.js', $deps = null, $ver = null, true );
		wp_enqueue_script( 'jquery' );

		// Main
		wp_enqueue_script( 'main-scripts', get_template_directory_uri() . '/assets/js/main.js', array( 'jquery' ), $this->wpTheme->get( 'Version' ), true );
		wp_localize_script( 'main-scripts', 'klflx_theme_settings', array(
			'version' => $this->wpTheme->get( 'Version' ),
			'ajaxUrl' => admin_url( 'admin-ajax.php' )
		) );
	}

	public function addInline( $js ) {
		$this->scripts[] = new ThemeScript( ThemeScript::Inline, $js );
	}

	public function addExternal( $url ) {
		$this->scripts[] = new ThemeScript( ThemeScript::External, $url );
	}

	/**
	 * Renders the dynamic scripts
	 */
	public function renderScripts() {

		$ret = '';

		// External scripts first
		foreach ( $this->scripts as $script ) {
			if($script->getType() == ThemeScript::Inline) {
				continue;
			}
			$ret .= $script->toHtml() . "\n";
		}

		// Inline script after
		foreach ( $this->scripts as $script ) {
			if($script->getType() == ThemeScript::External) {
				continue;
			}
			$ret .= $script->toHtml() . "\n";
		}

		if ( ! empty( $ret ) ) {
			echo $ret;
		}
	}

	public static function registerGlobalFunctions() {
		@include_once(__DIR__ . "../functions.php");
	}
}
