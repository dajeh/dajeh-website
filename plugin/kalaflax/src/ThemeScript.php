<?php

namespace Kalaflax;

class ThemeScript {

	const Inline = 0;
	const External = 1;

	private $type = null;

	public function __construct($type, $script) {
		if($type == self::External) {
			$this->type = self::External;
		} else {
			$this->type = self::Inline;
		}
		$this->script = $script;
	}

	public function toHtml() {
		if($this->type == self::External): ?>
			<script src="<?php echo $this->script; ?>" type="text/javascript"></script>
		<?php else: ?>
			<script type="text/javascript">
				(function($) {
					'use strict';
					<?php echo $this->script; ?>
				}(window.jQuery));
			</script>
		<?php endif;
	}

	/**
	 * @return int|null
	 */
	public function getType() {
		return $this->type;
	}
}
