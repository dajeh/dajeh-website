<?php

namespace Kalaflax\Utils;

class PhpUtils {

	private function __construct() {

	}

	/*
	 * Save method of checking an array property
	 */
	public static function getArrayValue( $array, $key, $defaultValue = '' ) {
        if (!is_array($array) || !array_key_exists($key, $array)) {
			return $defaultValue;
		}

		$value = $array[ $key ];

		if ( empty( $value ) ) {
			return $defaultValue;
		}

		return $value;
	}

	public static function addOrAppend( &$obj, $key, $value, $glue = ' ' ) {
		if ( is_array( $obj ) ) {
			if ( array_key_exists( $key, $obj ) ) {
				$obj[ $key ] .= $glue . $value;
			} else {
				$obj[ $key ] = $value;
			}
		} else if ( is_object( $obj ) ) {
			if ( property_exists( $obj, $key ) ) {
				$obj->$key .= $glue . $value;
			} else {
				$obj->$key = $value;
			}
		}
	}

	public static function recho( $something, $out ) {
		if ( $out ) {
			echo $something;
			return '';
		} else {
			return $something;
		}
	}
}
