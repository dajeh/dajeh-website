<?php

namespace Kalaflax\Utils;

class HtmlUtils {

	private function __construct() {

	}

	public static function getOptionalArrayAttr( $attr, $opts, $key = '', $default = '' ) {
		if ( empty( $key ) ) {
			$key = $attr;
		}
		$val = array_key_exists( $key, $opts ) ? $opts[ $key ] : $default;

		return self::getOptionalAttr( $attr, $val );
	}

	public static function getOptionalAttr( $attr, $value ) {
		if ( empty( $value ) ) {
			return '';
		}

		return self::getAttr( $attr, $value );
	}

	public static function getAttr( $key, $value ) {
		return $key . '="' . $value . '"';
	}

	public static function wrapStyles( $styles ) {
		return '<style scoped>' . $styles . '</style>';
	}

	public static function getTime( $datetime, $label, $opts = array() ) {
		$itemprop = '';
		if ( array_key_exists( 'itemprop', $opts ) ) {
			$itemprop = self::getAttr( 'itemprop', $opts['itemprop'] );
		}

		return '<time datetime="' . $datetime . '" ' . $itemprop . '>' . $label . '</time>';
	}

	public static function getPhoneLink( $phone, $opts = array() ) {

		PhpUtils::addOrAppend( $opts, 'class', 'lnk--icon lnk--phone' );
		PhpUtils::addOrAppend( $opts, 'iconClass', 'ficon-phone' );

		$class     = PhpUtils::getArrayValue( $opts, 'class' );
		$label     = PhpUtils::getArrayValue( $opts, 'label' );
		$iconClass = PhpUtils::getArrayValue( $opts, 'iconClass' );

		if ( ! empty( $label ) ) {
			$label = '<span class="lnk__lbl">' . $label . "</span>";
		}

		return '<a href="tel:' . $phone . '" class="' . $class . '"><i class="' . $iconClass . '"></i>' . $label . '</a>';
	}

	public static function img( $src, $attributes = array(), $lazy = false ) {
        if (!!$lazy) {
			// Prefix anything but 'class' with 'data'
			$attributes = array_map( function ( $attr ) {
				if ( strpos( $attr, 'class' ) === 0 || empty( $attr ) ) {
					return $attr;
				}

				return 'data-' . $attr;
			}, $attributes );

			$src = 'data-src="' . htmlspecialchars($src) . '"';
		} else {
			$src = HtmlUtils::getAttr( 'src',  $src );
		}

		return '<' . ( $lazy ? 'span' : 'img' ) . ' ' . $src . ' ' . implode( $attributes, ' ' ) . ( $lazy ? '></span>' : '>' );
	}

	public static function link( $url, $label, $opts = array() ) {
		$target = self::getOptionalArrayAttr( 'target', $opts );
		$class  = self::getOptionalArrayAttr( 'class', $opts );

		return '<a href="' . $url . '" ' . $class . ' ' . $target . '>' . $label . '</a>';
	}

	public static function toASCII( $str ) {
		$from = 'ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ';
		$to   = 'SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy';

		return strtr( utf8_decode( $str ), utf8_decode( $from ), $to );
	}

	public static function sluggify( $str ) {

	    if(empty($str)) {
	        return uniqid('beh-');
        }

		// Name to lower case.
		$str = mb_strtolower( $str );

		// Remove umlauts
		$str = str_replace( array( "ä", "ö", "ü" ), array( "ae", "oe", "ue" ), $str );

		$str = self::toASCII( $str );

		// Replace different characters with
		// a single hyphen
		return preg_replace( "/[\s_\-:+]+/", "-", $str );
	}
}
