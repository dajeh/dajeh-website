<?php

namespace Kalaflax\Utils;

use Kalaflax\Theme;
use Kalaflax\Walker\NavigationWalker;

class NavigationUtils {

	//
	// Constructor
	//
	private function __construct() {
		// empty
	}

	public static function getNavigation( $location, $opts = array() ) {

		$defaults = array(
			'container'      => '',
			'menu_class'     => '',
			'item_class'     => '',
			'link_class'     => '',
			'fallback_cb'    => '__return_false',
			'echo'           => true,
			'theme_location' => $location,
			'walker'         => new NavigationWalker()
		);

		$args = array_merge( $defaults, $opts );
		PhpUtils::addOrAppend( $args, 'menu_class', 'menu' );
		PhpUtils::addOrAppend( $args, 'item_class', 'menu__itm' );
		PhpUtils::addOrAppend( $args, 'link_class', 'menu__lnk' );

		return wp_nav_menu( $args );
	}

	//
	// Constructor
	//
	public static function registerFilters() {
		add_filter( 'nav_menu_css_class', array(
			'Kalaflax\Utils\NavigationUtils',
			'nav_menu_css_class_filter'
		), 10, 3 );
		add_filter( 'nav_menu_link_attributes', array(
			'Kalaflax\Utils\NavigationUtils',
			'nav_menu_link_attributes_filter'
		), 10, 4 );
	}

	//
	// Menu item classes
	//
	public static function nav_menu_css_class_filter( $classes, $item, $args ) {
		if ( property_exists( $args, 'item_class' ) ) {
			$classes[] = $args->item_class;
		}

		return $classes;
	}

	//
	// Menu item attributes
	//
	public static function nav_menu_link_attributes_filter( $atts, $item, $args ) {
		if ( property_exists( $args, 'link_class' ) ) {
			PhpUtils::addOrAppend( $atts, 'class', $args->link_class );
		}

		return $atts;
	}

	public static function getLanguageMenu( $opts = array() ) {

		$languages = apply_filters( 'wpml_active_languages', null, 'orderby=id&order=desc&skip_missing=0' );

		$nav      = '';
		$activeEl = '';
		foreach ( $languages as $key => $language ) {
			$active = ! ! $language['active'];
			$el     = self::getLocaleLink( $language );
			if ( $active ) {
				$activeEl = $el;
			} else {
				$nav .= $el;
			}
		}

		return $activeEl . '<div class="lang-options">' . $nav . '</div>';
	}

	private static function getLocaleLink( $language ) {

		$name   = sprintf( '%s (%s)', $language['native_name'], $language['translated_name'] );
		$active = ! ! $language['active'];

		$title = '';
		if ( ! $active ) {
			$title = ' title="' . sprintf( __( 'Switch language to %s', Theme::get()->getTextDomain() ), $name ) . '"';
		}

		return
			'<a class="lang ' . ( $active ? 'is-active' : '' ) . '" href="' . ( $active ? 'javascript:;' : $language['url'] ) . '"' . $title . '>'
			. '<i class="lang__icn icon-flag-' . $language['language_code'] . '"></i>'
			. ( $active ? '' : ' <span class="lang__lbl">' . $name . '</span>' )
			. '</a>';
	}

	public static function getCategoriesMenu( $out = false, $opts = array(), $parent = null, $depth = 0 ) {

		if ( $depth == 0 ) {
			$opts = array_merge( array( 'link_class' => '' ), $opts );
		}

		$cats = get_categories( array(
			'hide_empty' => 0,
			'pad_counts' => 1,
			'parent'     => $parent != null ? $parent->cat_ID : 0,
			'exclude'    => '1,7,11'
		) );

		$catsHtml = '';
		if ( $parent != null && ! empty( $cats ) ) {
			$catsHtml = '<li class="cat-menu__entry cat-menu__entry-all menu-item">' . self::getCategoryMenuEntry( $parent, $opts, sprintf( __( 'global.categoryOverview', 'klflx' ), $parent->name ) ) . '</li>';
		}

		$catOrderMap = array();
		foreach ( $cats as $cat ) {
			$order = get_field( 'order', 'category_' . $cat->cat_ID );
			if ( ! is_numeric( $order ) ) {
				$order = 0;
			}
			$catOrderMap[ $cat->cat_ID ] = $order;
		}

		usort( $cats, function ( $catOne, $catTwo ) use ( $catOrderMap ) {
			return $catOrderMap[ $catOne->cat_ID ] - $catOrderMap[ $catTwo->cat_ID ];
		} );

		foreach ( $cats as $cat ) {
			$children = self::getCategoriesMenu( false, $opts, $cat, $depth + 1 );
			$catLink  = self::getCategoryMenuEntry( $cat, $opts, '', $children );
			if ( ! empty( $catLink ) ) {
				$catsHtml .= '<li class="cat-menu__entry menu-item">' . $catLink . $children . '</li>';
			}
		}

		if ( ! empty( $catsHtml ) ) {
			if ( $parent != null ) {
				$catsHtml .= '<li class="cat-menu__entry menu-item"><a class="cat-menu__lnk back ' . $opts['link_class'] . ' js-cat-lnk js-cat-lnk--back" href="#"><i class="cat-menu__lnk-icon icon-arrow-left"></i>' . __( 'global.back', 'klflx' ) . '</a></li>';
			}
			$catsHtml = '<ul class="cat-menu__list js-cat-menu-lst cat-menu__list--lvl-' . $depth . '" data-level="' . $depth . '">' . $catsHtml . '</ul>';
		}

		if ( $out ) {
			echo $catsHtml;

			return '';
		} else {
			return $catsHtml;
		}
	}

	private static function getCategoryMenuEntry( $category, $opts, $name = '', $children = null ) {
		$catLink = get_category_link( $category );
		if ( empty( $catLink ) ) {
			return '';
		}

		if ( empty( $name ) ) {
			$name = $category->name;
		}

		return '<a class="cat-menu__lnk ' . $opts['link_class'] . ' ' . ( empty( $children ) ? '' : 'has-children js-cat-lnk--has-children' ) . ' js-cat-lnk" href="' . $catLink . '">' . $name . '<i class="cat-menu__lnk-icon icon-expand-cat"></i></a>';
	}

	public static function getPageUrl() {
		$pageURL = ( @$_SERVER["HTTPS"] == "on" ) ? "https://" : "http://";
		if ( $_SERVER["SERVER_PORT"] != "80" ) {
			$pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
		}

		return $pageURL;
	}
}
