<?php

namespace Kalaflax\Utils;

class SocialMediaUtils {

	public static function shareFacebook( $out = true ) {
		ThemeUtils::createShareUrl( 'https://www.facebook.com/sharer/sharer.php?u=%1$s', $out );
	}

	public static function shareTwitter( $out = true ) {
		ThemeUtils::createShareUrl( 'https://twitter.com/home?status=%1$s', $out );
	}

	public static function shareGoogleplus( $out = true ) {
		ThemeUtils::createShareUrl( 'https://plus.google.com/share?url=%1$s', $out );
	}

	public static function shareMail( $out = true ) {
		return PhpUtils::recho( 'mailto:my-friend@her-domain.com?subject=' . rawurlencode( get_the_title() ) . '&body=' . rawurlencode( NavigationUtils::getPageUrl() ), $out );
	}

}