<?php

namespace Kalaflax\Utils;

use Kalaflax\Theme;

class ThemeUtils
{

    public static function getTime($time = null, $out = null, $opts = [])
    {
        if (empty($time)) {
            $time = time();
        }

        $time = date_format('c', $time);
        $timeLabel = date_i18n(get_option('time_format'), $time);
        $result = HtmlUtils::getTime($time, $timeLabel, $opts);

        return PhpUtils::recho($result, $out);
    }

    public static function getDate($time, $out = true)
    {
        if (empty($time)) {
            $time = time();
        }

        $timestamp = date_i18n('c', $time);
        $dateLabel = date_i18n(get_option('date_format'), $time);
        $result = HtmlUtils::getTime($timestamp, $dateLabel);

        return PhpUtils::recho($result, $out);
    }

    public static function getDateTime($time, $out = true)
    {
        if (empty($time)) {
            $time = time();
        }

        $timestamp = date_i18n('c', $time);
        $dateLabel = date_i18n(get_option('date_format'), $time);
        $timeLabel = date_i18n(get_option('time_format'), $time);
        $label = sprintf(__('global.datetime', 'klflx'), $dateLabel, $timeLabel);
        $result = HtmlUtils::getTime($timestamp, $label);

        return PhpUtils::recho($result, $out);
    }

    public static function getPostTime($post = null, $out = true)
    {
        $post = self::getPost($post);
        $timestamp = get_the_date('c', $post);
        $label = get_the_date(get_option('date_format'), $post);
        $result = HtmlUtils::getTime($timestamp, $label, ['itemprop' => 'datePublished']);

        return PhpUtils::recho($result, $out);
    }

    public static function getCommentTime($comment = null, $out = true)
    {
        $comment = self::getComment($comment);
        $time = strtotime($comment->comment_date);
        $timestamp = date_i18n('c', $time);
        $dateLabel = date_i18n(get_option('date_format'), $time);
        $timeLabel = date_i18n(get_option('time_format'), $time);
        $label = sprintf(__('global.datetime', 'klflx'), $dateLabel, $timeLabel);
        $result = HtmlUtils::getTime($timestamp, $label);

        return PhpUtils::recho($result, $out);
    }

    public static function getCommentMeta($comment = null, $out = true)
    {
        $comment = self::getComment($comment);
        $commentTime = self::getCommentTime($comment, false);

        return PhpUtils::recho(sprintf(__('By %1$s on %2$s', 'klflx'), get_comment_author_link($comment), $commentTime), $out);
    }

    public static function getFeaturedImage($post = null, $opts = [], $out = true)
    {
        return self::getPostThumbnail($post, $opts, $out);
    }

    public static function getPostThumbnail($post = null, $opts = [], $out = true)
    {
        $post = self::getPost($post);

        if ($post == null) {
            return '';
        }

        if (!has_post_thumbnail($post->ID)) {
            return '';
        }

        $imageId = get_post_thumbnail_id($post->ID);
        if ($imageId) {
            return PhpUtils::recho(ImageUtils::getImage($imageId, $opts), $out);
        }

        return '';
    }

    public static function getPost($_post = null)
    {
        global $post;

        if ($_post != null) {
            return $_post;
        }

        if ($post != null) {
            return $post;
        }

        return null;
    }

    public static function getComment($_comment = null)
    {
        global $comment;

        if ($_comment != null) {
            return $_comment;
        }

        if ($comment != null) {
            return $comment;
        }

        return null;
    }

    public static function createShareUrl($shareUrl, $out)
    {
        $pageUrl = urlencode(NavigationUtils::getPageUrl());
        $pageTitle = get_the_title();
        PhpUtils::recho(sprintf($shareUrl, $pageUrl, $pageTitle), $out);
    }

    public static function getCommentCount($_post = null, $out = true)
    {
        $post = self::getPost($_post);
        $commentCount = get_comments_number($post);
        $nx = _n('One comment', '%1$s comments', $commentCount, Theme::get()->getTextDomain());
        PhpUtils::recho(sprintf($nx, number_format_i18n($commentCount)), $out);
    }

    public static function getTags($out = true)
    {
        $posttags = get_the_tags();
        if (empty($posttags)) {
            return '';
        }

        $tags = '';
        foreach ($posttags as $tag) {
            $tags .= ' <a class="tag-lnk" href="' . get_tag_link($tag->term_id) . '">#' . $tag->name . '</a>';
        }

        return PhpUtils::recho($tags, $out);
    }

    public static function getInfoBlock($headline, $body, $modifier = '', $class = '')
    {
        $modifier = implode(' ', array_map(function ($el) {
            return 'info-block--' . $el;
        }, explode(' ', $modifier)));

        return '<section class="info-block ' . $modifier . ' ' . $class . '"><header class="info-block__hd"><h2 class="info-block__ttl">' . $headline . '</h2></header>' .
            '<div class="info-block__bd">' . $body . '</div></section>';
    }

    public static function newLineToParagraph($s)
    {
        $result = '';
        $paragraphs = explode("\n", $s);
        foreach ($paragraphs as $paragraph) {
            $trimmed = trim($paragraph);
            if (!empty($trimmed)) {
                $result .= '<p>' . $paragraph . '</p>';
            }
        }

        return $result;
    }

    public static function newLineToBreak($s)
    {
        $result = '';
        $paragraphs = explode("\n", $s);
        foreach ($paragraphs as $paragraph) {
            $trimmed = trim($paragraph);
            if (!empty($trimmed)) {
                $result .= $paragraph . '<br>';
            }
        }

        return $result;
    }

    public static function wrapLatin($s)
    {
        return preg_replace('/[\p{Latin}[A-Za-z]+/imu', '<span class="latin">$0</span>', $s);
    }

    public static function getVersionedUrl($url, $out = false)
    {
        return PhpUtils::recho(self::addVersionParam(get_template_directory_uri() . $url), $out);
    }

    public static function addVersionParam($url)
    {
        $version = wp_get_theme()->get('Version');
        return "$url?ver=$version";
    }
}
