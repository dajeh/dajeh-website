<?php

namespace Kalaflax\Utils;

class ImageUtils
{

    private function __construct()
    {

    }

    /**
     * Generates a responsive image using picturefill
     *
     * @param $_image
     * @param array $opts
     *
     * @return string
     */
    public static function getImage($_image, $opts = [])
    {

        $image = null;
        if (is_numeric($_image) || is_string($_image)) {
            $image = get_post($_image);
        } else if (is_object($_image) && $_image instanceof \WP_Post) {
            $image = $_image;
        }

        if ($image == null) {
            return '';
        }

        // Gather infos
        $imgMeta = wp_get_attachment_metadata($image->ID);
        $uploadUrl = wp_upload_dir()['baseurl'] . '/' . dirname($imgMeta['file']) . '/';

        if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
            $uploadUrl = str_replace('http://', 'https://', $uploadUrl);
        }

        $imageStack = [];
        // Add full image
        $imageStack[$imgMeta['width']] = [
            'url' => $uploadUrl . basename($imgMeta['file']),
            'height' => $imgMeta['height']
        ];

        // Add all scaled images
        foreach ($imgMeta['sizes'] as $name => $img) {
            if ($name == 'thumbnail' || $name == 'post-thumbnail') {
                continue;
            }

            $url = $uploadUrl . $img['file'];
            $width = $img['width'];
            $height = $img['height'];
            $imageStack[$width] = [
                'url' => $url,
                'height' => $height
            ];
        }

        // Sort image ascending
        ksort($imageStack);

        // Add meta values
        $opts = array_merge($opts, [
            'title' => $image->post_title,
            'alt' => get_post_meta($image->ID, '_wp_attachment_image_alt', true)
        ]);

        // Return
        if (array_key_exists('bg', $opts) && $opts['bg']) {
            return self::getBgImageForStack($imageStack, $opts);
        } else {
            return self::getImageForStack($imageStack, $opts);
        }
    }

    /*
     * Generates a responsive image either as <img> with sourceset element or as style
     */
    public static function getACFImage($acfImage, $opts = [])
    {
        $imageSizes = $acfImage['sizes'];
        if (empty($imageSizes)) {
            return '<!-- no image found -->';
        }

        $imageStack = [];
        $imageSizesValues = array_values($imageSizes);
        for ($i = 0; $i < count($imageSizes); $i += 3) {
            $url = $imageSizesValues[$i];
            $width = $imageSizesValues[$i + 1];
            $height = $imageSizesValues[$i + 2];
            $imageStack[$width] = [
                'url' => $url,
                'height' => $height
            ];
        }
        ksort($imageStack);

        $opts = array_merge($opts, [
            'alt' => PhpUtils::getArrayValue($acfImage, 'alt'),
            'title' => PhpUtils::getArrayValue($acfImage, 'title')
        ]);

        if (array_key_exists('bg', $opts) && $opts['bg']) {
            return self::getBgImageForStack($imageStack, $opts);
        } else {
            return self::getImageForStack($imageStack, $opts);
        }
    }

    public static function getAssetImage($_imageStack, $opts = [])
    {
        $imageStack = [];
        $basePath = '/assets/images/';

        if (is_array($_imageStack)) {
            foreach ($_imageStack as $width => $file) {
                $imageStack[$width] = [
                    'url' => ThemeUtils::getVersionedUrl($basePath . $file, false)
                ];
            }
        }

        if (array_key_exists('bg', $opts) && $opts['bg']) {
            return self::getBgImageForStack($imageStack, $opts);
        } else {
            return self::getImageForStack($imageStack, $opts);
        }
    }

    public static function getUploadedImage($_imageStack, $opts = [])
    {
        $imageStack = [];
        $basePath = '/wp-content/uploads/';

        foreach ($_imageStack as $width => $file) {
            $imageStack[$width] = [
                'url' => $basePath . $file
            ];
        }

        return self::getImageForStack($imageStack, $opts);
    }

    /*
     * Generates a SVG image tag with png fallback
     */
    public static function getSVG($svg, $options = [], $out = true)
    {

        if (is_string($options)) {
            $options = ['title' => $options];
        }

        PhpUtils::addOrAppend($options, 'class', 'svg svg-' . $svg);
        $svgUrl = ThemeUtils::getVersionedUrl('/assets/images/symbols.svg', false) . '#' . $svg;

        $class = HtmlUtils::getOptionalArrayAttr('class', $options);
        $title = HtmlUtils::getOptionalArrayAttr('title', $options);
        $alt = HtmlUtils::getOptionalArrayAttr('alt', $options);

        $svg = "<svg role=\"img\" $title $class $alt>
	        <use xlink:href=\"$svgUrl\"/>
	    </svg>";

        return PhpUtils::recho($svg, $out);
    }

    /*
     * Generates an inline SVG image tag with png fallback
     */
    public static function inlineSVG($svg, $fallbackStack = null, $options = [])
    {
        $basePath = '/assets/images/';
        echo '<span class="inline-svg-ct js-inline-svg-ct">';

        $svgPath = get_template_directory() . $basePath . $svg;

        if (@file_exists($svgPath)) {
            echo @file_get_contents($svgPath);
        }

        if ($fallbackStack) {
            $options['lazy'] = true;
            PhpUtils::addOrAppend($options, 'class', 'js-inline-svg-fb');
            echo self::getAssetImage($fallbackStack, $options);
        }

        echo '</span>';
    }

    private static function getBgImageForStack($imageStack, $opts = [])
    {

        $selector = PhpUtils::getArrayValue($opts, 'selector', '');
        if (empty($selector)) {
            error_log("No selector for background image given. Aborting.'");

            return '';
        }

        $noCssOptions = PhpUtils::getArrayValue($opts, 'noCssOptions', false);
        $position = PhpUtils::getArrayValue($opts, 'position', '50% 50%');
        $repeat = PhpUtils::getArrayValue($opts, 'repeat', 'no-repeat');
        $size = PhpUtils::getArrayValue($opts, 'size', 'cover');

        $style = '';

        $first = true;
        $lastWidth = 0;
        foreach ($imageStack as $width => $img) {
            if (!$first) {
                $style .= '@media (min-width: ' . (intval($lastWidth) + 1) . 'px) {';
            }
            $style .= $selector . ' { ' .
                'background-image: url("' . $img['url'] . '"); ';
            if ($first && !$noCssOptions) {
                $style .= 'background-position:' . $position . ';' .
                    'background-repeat:' . $repeat . ';' .
                    'background-size:' . $size . ';';
            }
            $style .= '}';
            if (!$first) {
                $style .= '}';
            }
            $first = false;
            $lastWidth = $width;
        }

        return '<style>' . $style . '</style>';
    }

    private static function getImageForStack($imageStack, $opts = [])
    {
        // Grab first of the stack
        $baseImg = array_values($imageStack)[0];
        $baseImgUrl = $baseImg['url'];
        $baseImgHeight = PhpUtils::getArrayValue($baseImg, 'height', 0);
        $baseImgWidth = array_keys($imageStack)[0];

        // Grab last image of the stack
        $origImg = array_values($imageStack)[count($imageStack) - 1];
        $origImgUrl = $origImg['url'];
        $origImgHeight = PhpUtils::getArrayValue($origImg, 'height', 0);
        $origImgWidth = array_keys($imageStack)[count($imageStack) - 1];

        // Lazy load image
        $lazy = array_key_exists('lazy', $opts) && !!$opts['lazy'];
        if ($lazy) {
            PhpUtils::addOrAppend($opts, 'class', 'img--lazy js-img-lazy');
        }

        $imgAttr = [];

        // Iterate over the rest
        $srcset = '';
        foreach ($imageStack as $width => $img) {
            $srcset .= ' ' . $img['url'] . ' ' . $width . 'w,';
        }
        $srcset = substr($srcset, 0, strlen($srcset) - 1);

        if (array_key_exists('width', $opts) && array_key_exists('height', $opts)) {
            $imgAttr[] = HtmlUtils::getOptionalArrayAttr('width', $opts);
            $imgAttr[] = HtmlUtils::getOptionalArrayAttr('height', $opts);
        } else {
            $imgAttr[] = HtmlUtils::getAttr('width', $baseImgWidth);
            if (is_int($baseImgHeight) && $baseImgHeight > 0) {
                $imgAttr[] = HtmlUtils::getAttr('height', $baseImgHeight);
            }
        }

        if (array_key_exists('itemprop', $opts)) {
            $imgAttr[] = HtmlUtils::getAttr('itemprop', 'image');
        }

        $imgAttr[] = HtmlUtils::getAttr('srcset', $srcset);
        $imgAttr[] = HtmlUtils::getOptionalArrayAttr('class', $opts);
        $imgAttr[] = HtmlUtils::getOptionalArrayAttr('alt', $opts);
        $imgAttr[] = HtmlUtils::getOptionalArrayAttr('title', $opts);
        $imgAttr[] = HtmlUtils::getOptionalAttr('data-orig', $origImgUrl);
        $imgAttr[] = HtmlUtils::getOptionalAttr('data-height', $origImgHeight);
        $imgAttr[] = HtmlUtils::getOptionalAttr('data-width', $origImgWidth);
        $imgAttr[] = self::getSizes($opts);

        return HtmlUtils::img($baseImgUrl, $imgAttr, $lazy);
    }

    /**
     * Computes the sizes attribute of the an <image>
     *
     * @param $opts array media query to width map
     *
     * @return string
     */
    public static function getSizes($opts)
    {

        if (!array_key_exists('sizes', $opts)) {
            return 'sizes="100vw"';
        }

        $_sizes = $opts['sizes'];
        if (empty($_sizes)) {
            return 'sizes="100vw"';
        }

        $sizes = [];
        $hasDefault = false;

        foreach ($_sizes as $query => $width) {

            if (is_int($query)) {
                $min = $query;
                $max = false;
            } else if (is_string($query)) {
                list($min, $max) = explode('-', $query);
                $min = intval($min);
                $max = intval($max);
            } else {
                continue;
            }

            if ($min === 0) {
                $sizes[] = $width;
                $hasDefault = true;
                continue;
            }

            if ($min !== $max) {
                $sizesQuery = '';
                if ($min != 0) {
                    $sizesQuery .= '(min-width: ' . $min . 'px)';
                    if ($max) {
                        $sizesQuery .= ' and (max-width: ' . $max . 'px)';
                    }
                } else {
                    $sizesQuery = '(max-width: ' . $max . 'px)';
                }

                $sizes[] = $sizesQuery . ' ' . $width;
            }
        }

        if (!$hasDefault) {
            array_unshift($sizes, '100vw');
        }

        return 'sizes="' . implode(', ', array_reverse($sizes)) . '"';
    }

    public static function getAssetImageUrl($path, $addVersion = true)
    {
        $url = trailingslashit(get_template_directory_uri()) . 'assets/images/' . $path;
        return $addVersion ? ThemeUtils::addVersionParam($url) : $url;
    }

    public static function getAssetVideoUrl($path, $addVersion = true)
    {
        $url = trailingslashit(get_template_directory_uri()) . 'assets/videos/' . $path;
        return $addVersion ? ThemeUtils::addVersionParam($url) : $url;
    }
}
