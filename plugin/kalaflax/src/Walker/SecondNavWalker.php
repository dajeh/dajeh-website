<?php

namespace Kalaflax\Walker;

/**
 * Ceiltec and second navigation walker
 */
class SecondNavWalker extends \Walker_Nav_Menu {

	private static $POST_IDS = [];

	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		if ( empty( $item->classes ) ) {
			$item->classes = array();
		}

		$item->classes[]  = 'page__sub-nav-itm';
		self::$POST_IDS[] = $item->object_id;
		parent::start_el( $output, $item, $depth, $args, $id );
	}

	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat( "\t", $depth );
		$output .= "\n$indent <ul class=\"page__sub-nav-lst\">\n";
	}

	public function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat( "\t", $depth );
		$output .= "$indent</ul>\n";
	}

	// Only follow down one branch
	public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {

		if ( ! $element ) {
			return;
		}

		parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
	}

	public static function contains( $id ) {
		return in_array( $id, self::$POST_IDS );
	}

	public static function getIds() {
		return self::$POST_IDS;
	}
}
