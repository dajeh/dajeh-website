<?php

namespace Kalaflax\Walker;

/*
 * Navigation walker
 */
class NavigationWalker extends \Walker_Nav_Menu {

	public function start_lvl(&$output, $depth = 0, $args = array()) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent <div class=\"sub-menu-ct js-nv-sub-mn-ct\"><ul class=\"nav__menu menu menu--sub nav__menu--sub js-nv-sub-mn\">\n";
	}

	public function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul></div>\n";
	}

	// Only follow down one branch
	public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {

		if ( ! $element ) {
			return;
		}

		parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
	}
}
