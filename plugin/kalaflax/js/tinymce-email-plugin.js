/**
 * TinyMCE E-Mail button plugin
 */

tinymce.PluginManager.add('klflx_email', function (editor, url) {

    function submit(e) {

        var encodedSubject = encodeURIComponent(e.data.subject);
        var encodedBody = encodeURIComponent(e.data.message);
        var mailtoLink = "mailto:" + e.data.email + "?subject=" + encodedSubject + "&body=" + encodedBody;

        // Insert content when the window form is submitted
        editor.insertContent('<a href="' + mailtoLink + '">' + e.data.title + '</a>');
    }

    function openWindow() {

        editor.windowManager.open({
            title: 'Mail-To Konfigurator',
            body: [
                {
                    name: 'title',
                    type: 'textbox',
                    label: 'Titel'
                },
                {
                    name: 'email',
                    type: 'textbox',
                    label: 'E-Mail',
                    value: 'info@kalaflax.de'
                },
                {
                    name: 'subject',
                    type: 'textbox',
                    label: 'Betreff'
                },
                {
                    name: 'message',
                    type: 'textbox',
                    label: 'Nachricht',
                    multiline: true
                }
            ],
            onsubmit: submit
        });
    }

    // Add a button that opens a window
    editor.addButton('klflx_email', {
        text: 'E-Mail',
        icon: false,
        onclick: openWindow
    });

    // Adds a menu item to the tools menu
    editor.addMenuItem('klflx_email', {
        text: 'E-Mail',
        context: 'tools',
        onclick: openWindow
    });
});
