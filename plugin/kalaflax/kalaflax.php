<?php
/*
Plugin Name: Kalaflax Plugin
Plugin URI: https://www.kalaflax.com/
Description: Plugin
Version: 2.2.1
Author: Kalaflax GmbH
Author URI: http://www.kalaflax.com/
Copyright: Kalaflax GmbH
Text Domain: kalaflax
*/

namespace Kalaflax;

if (!defined('ABSPATH')) exit;

define('KLFLX_PLUGIN_VERSION', '2.2.1');

require_once 'autoload.php';
require_once 'functions.php';
