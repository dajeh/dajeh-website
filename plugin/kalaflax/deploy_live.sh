#!/bin/sh

SCRIPTPATH=$( cd $(dirname $0) ; pwd -P )

# Exclude file
EXCLUDE_FILE="${SCRIPTPATH}/rsync_exclude.txt"

# Directory to sync with live
SYNC_DIR="${SCRIPTPATH}"

# Host
HOST=kalaflax-web-live

# User
USER=root

# Path on host
HOST_PATH=/var/www/vhosts/kalaflax.com/httpdocs/wp-content/plugins/kalaflax/

#
# Deploy script for live
#
# -a             = archive mode
# -v             = verbose
# -z             = compress
# --delete       = delete unkown files from dest during sync
# --stats        = file transfer stats
# --exclude-from = exlusion list
#
# --dry-run      = run simulation
#
/usr/local/bin/rsync -rlDvz \
	--delete \
	--stats \
	-og --chown=kalaflax:psacln \
	--exclude-from ${EXCLUDE_FILE} \
	${SYNC_DIR}/ \
	${USER}@${HOST}:${HOST_PATH}
