<?php

namespace {

    use Kalaflax\Theme;
    use Kalaflax\Utils\HtmlUtils;
    use Kalaflax\Utils\ImageUtils;
    use Kalaflax\Utils\NavigationUtils;
    use Kalaflax\Utils\PhpUtils;
    use Kalaflax\Utils\ThemeUtils;

    /*
     * Scaffolding
     */
	function klflx_set_page_id( $pageId ) {
		Theme::get()->setPageId( $pageId );
	}

	/*function klflx_render_module( $key, $opts = array() ) {
		echo ModuleManager::get()->render( $key, $opts );
	}*/

	/*
	 * Loop related
	 */
	function klflx_get_by_category( $post_type, $count = 5 ) {
		/*
		 * Custom function for serializing a category from url GET parameters.
		 */
		wp_reset_postdata();

		global $args;

		if (isset($_GET[ 'category' ])) {
			$category = $_GET[ 'category' ];

			$args = array(
				'post_type' => $post_type,
				'posts_per_page' => $count,
				'category' => $category
			);
		} else {
			// in the default scenario no category is chosen
			$args = array(
				'post_type' => $post_type,
				'posts_per_page' => $count,
			);
		}

		return new CustomQuery($post_type, true, $args);
	}

	function klflx_print_categories( $taxonomy, $post_type ) {
		$categories = get_categories( array('taxonomy' => $taxonomy) );

		if ( isset($categories) ) {
			foreach ($categories as $category) {

				echo '<li><a href="' . site_url( $post_type ) . '/?category=' . $category->slug . '"';

				// mark the active category
				if ( isset($_GET[ 'category' ]) ) {
					if ( $_GET[ 'category' ] == $category->slug ) {
						echo ' class="' . $taxonomy . '_nav__active"';
					}
				}

				echo '>' . $category->name . '</a></li>';
			}
		}
	}

	/**
	 * Generates a unique id which can be used
	 * as HTML id or class
	 */
	function klflx_id() {
		return uniqid('klflx-');
	}

	/*
	 * Navigation
	 */
	function klflx_get_navigation( $navKey, $opts = array() ) {
		return NavigationUtils::getNavigation( $navKey, $opts );
	}

	function klflx_navigation( $navKey, $opts = array() ) {
		echo klflx_get_navigation( $navKey, $opts );
	}

	function klflx_theme_url( $out = true ) {
		return PhpUtils::recho( get_template_directory_uri(), $out );
	}

	function klflx_template_dir( $out = true ) {
		return PhpUtils::recho( get_template_directory(), $out );
	}

	function klflx_post_time( $post = null ) {
		ThemeUtils::getPostTime( $post );
	}

	function klflx_comment_time( $comment = null ) {
		ThemeUtils::getCommentTime( $comment );
	}

	function klflx_comment_meta( $comment = null ) {
		ThemeUtils::getCommentMeta( $comment );
	}

	function klflx_featured_img( $post = null, $opts = array(), $output = true ) {
		return ThemeUtils::getFeaturedImage( $post, $opts, $output );
	}

	function klflx_info_block( $headline, $body, $modifier = '', $class ) {
		echo ThemeUtils::getInfoBlock( $headline, $body, $modifier, $class );
	}

	function klflx_nl2p( $s, $out = true ) {
		return PhpUtils::recho( ThemeUtils::newLineToParagraph( $s ), $out );
	}

	function klflx_nl2br( $s, $out = true ) {
		return PhpUtils::recho( ThemeUtils::newLineToBreak( $s ), $out );
	}

	function klflx_url( $url, $out = true ) {
		return PhpUtils::recho( get_template_directory_uri() . $url . '?ver=' . wp_get_theme()->get( 'Version' ), $out );
	}

	function klflx_phone_lnk( $phone = null, $opts = array() ) {
		echo HtmlUtils::getPhoneLink( $phone, $opts );
	}



	function klflx_wrap_latin( $s ) {
		return ThemeUtils::wrapLatin( $s );
	}

    /*
     * ACF functions
     */
    function klflx_the_field($selector, $post_id = false, $default = false)
    {
        echo klflx_get_field($selector, $post_id, $default);
    }

    function klflx_get_field($selector, $post_id = false, $default = false)
    {
        $value = get_field($selector, $post_id);
        return (empty($value) && $default ? $default : $value);
    }

    function klflx_get_blog_url($categoryIds = null)
    {
        if (empty($categoryIds) || count($categoryIds) > 1) {
            return get_home_url();
        }

        $categoryId = $categoryIds[0];
        return get_category_link($categoryId);
    }

    function klflx_get_latest_posts($limit = 0, $categoryIds = null)
    {
        $posts = [];
        $args = [
            'post_type' => 'post',
            'post_status' => 'publish',
        ];

        if ($limit == 'all') {
            $limit = 0;
        }

        $limit = intval($limit);

        if ($limit > 0) {
            $args['posts_per_page'] = $limit;
        }

        if (!empty($categoryIds)) {
            $args['cat'] = join(',', $categoryIds);
        }

        query_posts( $args );

        if ( have_posts() ) {
            while (have_posts()) {
                the_post();

                $data = [
                    'title' => get_the_title(),
                    'url' => get_permalink(),
                    'excerpt' => get_the_excerpt(),
                    'published' => get_the_date(),
                    'image' => get_the_post_thumbnail()
                ];

                $categories = get_the_category();
                if (count($categories)) {
                    $categoryArray = [];
                    foreach ($categories as $category) {
                        if($categories[0]->term_id == 1) {
                            continue;
                        }
                        $url = esc_url(get_category_link($category));
                        $name = $category->name;
                        $categoryArray[] = "<a href=\"$url\">$name</a>";
                    }
                    $data['categories'] = implode(', ', $categoryArray);
                }

                $posts[] = $data;
            }
        }

        if ($limit > 0 && $limit < count($posts)) {
            $posts = array_slice($posts, count($posts) - $limit, $limit);
        }

        wp_reset_query();

        return $posts;
    }

	/*
	 * Image functions
	 */

	function klflx_svg( $svg, $options = array() ) {
		ImageUtils::getSVG( $svg, $options );
	}

	function klflx_inline_svg( $svg, $fallback = null, $options = array() ) {
		ImageUtils::inlineSVG( $svg, $fallback, $options );
	}

	function klflx_img_acf( $acfImage, $opts = array() ) {
		echo ImageUtils::getACFImage( $acfImage, $opts );
	}

	function klflx_asset_img( $stack, $options = array() ) {
		echo ImageUtils::getAssetImage( $stack, $options );
	}

	function klflx_asset_img_url( $path ) {
		echo ImageUtils::getAssetImageUrl( $path );
	}

    function klflx_asset_video_url( $path ) {
        echo ImageUtils::getAssetVideoUrl( $path, false );
    }

	function klflx_img_upload( $stack, $options = array() ) {
		echo ImageUtils::getUploadedImage( $stack, $options );
	}


    /**
	 * @param $n
	 *
	 * @return bool
	 */
	function isOdd( $n ) {
		return $n % 2 == 0;
	}

	/**
	 * Returns all values by a given `key` from a nested `array` as
	 * array.
	 *
	 * @param $array
	 *
	 * @return array
	 */
	function get_values_recursively( $array, $key ) {
		$items = [];

		if ( ! is_array( $array ) || empty( $array ) ) {
			return $items;
		}

		array_walk_recursive( $array, function ( $a, $b ) use ( &$items, $key ) {
			if ( $b === $key ) {
				$items[] = $a;
			}
		} );

		return array_filter( $items, function ( $n ) {
			return $n !== '';
		} );
	}

	function klflx_print_navtitle( $navtitle ) {
		if ( $navtitle !== '' ) {
			echo 'id="' . $navtitle . '"';
		} else {
			echo "";
		}
	}


	/*
	 * Template functions
	 */
	function klflx_module_image( $imageItem,  $opts = array() ) {
        $showCaption = PhpUtils::getArrayValue($opts, 'show_caption', false);
		$border = PhpUtils::getArrayValue($opts, 'border', false);
		$imgClasses = PhpUtils::getArrayValue($opts, 'img_class', '');

		$caption = $imageItem['caption'];
		if(empty($caption)) {
			$caption = $imageItem['image']['caption'];
		}
		$hasCaption = !empty($caption);

		if($showCaption && $hasCaption) {
		    $imgClasses .= ' figure__img';
		    echo '<figure class="figure">';
        } else if ($border) {
			$imgClasses .= ' img--border';
        }

        klflx_img_acf($imageItem['image'], array('class' => $imgClasses));

		if($showCaption && $hasCaption) {
		    echo '<figcaption class="figure__caption">';
            echo $caption;
			echo "</figcaption>";
			echo "</figure>";
		}
	}

	function klflx_module_image_slider( $images, $opts = array() ) {
		?>
		<ul class="image-slider js-img-slider">
			<?php foreach ( $images as $image ): ?>
				<li class="image-slider__itm">
					<?php klflx_module_image($image, $opts); ?>
				</li>
			<?php endforeach; ?>
		</ul>
		<?php
	}

	function klflx_reference_taxonomies($referenceIds = null) {

	    global $wpdb;

	    if($referenceIds == null) {
		    global $wp_query;
		    $referenceIds = array_map( function ( $post ) {
			    return $post->ID;
		    }, array_filter( $wp_query->posts, function ( $post ) {
			    return $post->post_type == 'reference';
		    } ) );
	    }

	    if(empty($referenceIds)) {
		    return array();
	    }

	    $postTaxonomiesMap = array();

        /*
         * Fetch and add terms
         */
		$referenceIdsParam = join(",", $referenceIds);
        $terms = $wpdb->get_results("
          SELECT post_terms.object_id AS post_id, terms.term_id AS id, terms.name AS name, terms.slug AS slug, term_taxonomies.taxonomy AS taxonomy 
          FROM $wpdb->term_relationships AS post_terms 
            LEFT JOIN $wpdb->terms AS terms ON post_terms.term_taxonomy_id = terms.term_id
             LEFT JOIN $wpdb->term_taxonomy AS term_taxonomies ON term_taxonomies.term_id = terms.term_id
          WHERE post_terms.object_id IN ($referenceIdsParam)
          ", ARRAY_A);

		/*
		 * Build taxonomy list
		 */
		foreach($terms as $term) {

			// Check if post already exist
			if(!array_key_exists($term['post_id'], $postTaxonomiesMap)) {
				$postTaxonomiesMap[$term['post_id']] = array();
			}

			$taxonomies = $postTaxonomiesMap[$term['post_id']];

			// Check if taxonomy already exist
			if(!array_key_exists($term['taxonomy'], $taxonomies)) {
				$taxonomies[$term['taxonomy']] = array();
			}

			$taxonomies[$term['taxonomy']][] = $term;

			// Save
			$postTaxonomiesMap[$term['post_id']] = $taxonomies;
		}

        return $postTaxonomiesMap;
    }

    function klflx_prev_post() {
        return get_adjacent_post( false, false, true );
    }

    function klflx_next_post() {
	    return get_adjacent_post( false, false, false );
    }

    function klflx_get_page_template() {
	    $pageTemplateFile = get_page_template();
	    if(empty($pageTemplateFile)) {
	        return null;
	    }

	    $templateDir = get_template_directory();

	    if(strcmp(substr($pageTemplateFile, 0, strlen($templateDir)), $templateDir) == 0) {
		    return substr($pageTemplateFile, strlen($templateDir) + 1, strlen($pageTemplateFile));
	    }

	    return null;
    }

	function klflx_is_page_template($template) {
	    return strcmp($template, klflx_get_page_template()) == 0;
	}

	function klflx_is_ceiltec_page($includeChildren = false) {
		return klflx_is_page(get_field('ceiltec-page', 'option'), $includeChildren);
	}

	function klflx_is_about_page($includeChildren = false) {
		return klflx_is_page(get_field('about-us-page', 'option'), $includeChildren);
	}

	function klflx_is_page($pageId, $includeChildren = false) {
		global $post;

		if(!is_object($post)) {
			return false;
		}

		if($post->post_type != 'page') {
			return false;
		}

		if($post->ID == $pageId) {
			return true;
		}

		if(!$includeChildren) {
			return false;
		}

		$pageAncestors = get_ancestors( $post->ID, 'page', 'post_type' );
		if(in_array($pageId, $pageAncestors)) {
			return true;
		}

		return false;
    }

    /**
     * @return string|false the current post type name or false
     */
    function klflx_get_archive_post_type() {
	    global $wp_query;

        if(array_key_exists('post_type', $wp_query->query)) {
            return $wp_query->query['post_type'];
        }

        return false;
    }

    /**
     * Determines the taxonomies for the given post type
     */
    function klflx_get_post_type_taxonomies($postType, $ignoredTaxonomies = array()) {

        global $wp_taxonomies;

        $taxonomies = $wp_taxonomies;

        /*
         * Determine taxonomies
         */
        $taxonomies = array_filter($taxonomies, function ($taxonomy) use ($postType) {
            return in_array($postType, $taxonomy->object_type);
        });

        // Filter ignored ones
        if (!empty($ignoredTaxonomies)) {
            $taxonomies = array_filter($taxonomies, function ($taxonomy) use ($ignoredTaxonomies) {
                return !in_array($taxonomy->name, $ignoredTaxonomies);
            });
        }

        return $taxonomies;
    }

    function klflx_get_where_in_placeholder($objects, $singlePlaceholder = '%d') {
        return implode(', ', array_fill(0, count($objects), $singlePlaceholder));
    }

	function klflx_get_post_type_terms($postType, $taxonomies = false) {

	    global $wpdb;

	    if(!$taxonomies) {
	        $taxonomies = array_keys(klflx_get_post_type_taxonomies($postType));
        }

        $taxonomiesPlaceholder = klflx_get_where_in_placeholder($taxonomies, '%s');

	    $queryArgs = array();
        $queryArgs[] = $postType;
        $queryArgs = array_merge($queryArgs, $taxonomies);

        $termIds = $wpdb->get_results(
			$wpdb->prepare("
			        SELECT DISTINCT(wp_terms.term_id) FROM wp_posts 
                    LEFT JOIN wp_term_relationships ON wp_posts.ID = wp_term_relationships.object_id
                    LEFT JOIN wp_terms ON wp_term_relationships.term_taxonomy_id = wp_terms.term_id
                    LEFT JOIN wp_term_taxonomy ON wp_terms.term_id = wp_term_taxonomy.term_id
                    WHERE wp_posts.post_status = 'publish'
                    AND wp_posts.post_type = %s
                    AND wp_term_taxonomy.taxonomy IN ($taxonomiesPlaceholder)
                    ",
                $queryArgs
            ), ARRAY_N);

		$termIds = array_map(function($resultRow) {
			return (int) $resultRow[0];
		}, $termIds);

		return get_terms(array(
            'include' => $termIds
        ));
    }

    function klflx_pagination()
    {
        global $wp_query;

        $current_page = max(1, get_query_var('paged'));
        $total_posts = $wp_query->found_posts;
        $total_pages = max(1, $wp_query->max_num_pages);

        $posts = sprintf(__('%1$d Einträge', 'kalaflax'), $total_posts);
        $current = sprintf(__('Seite %1$d von %2$d', 'kalaflax'), $current_page, $total_pages);
        $first = get_pagenum_link(1);
        $previous = get_pagenum_link(max(1, $current_page - 1));
        $next = get_pagenum_link(min($total_pages, $current_page + 1));
        $last = get_pagenum_link(max(1, $total_pages));
        ?>
        <div class="posts__pagination-ct">
            <nav class="posts__pagination">
                <a href="<?php echo $first ?>" title="<?php _e('Erste Seite', 'kalaflax'); ?>" class="posts__pagination-first btn btn--icon">
                    <i class="icon-first-page"></i>
                    <span class="screen-reader"><?php _e('Erste Seite', 'kalaflax'); ?></span>
                </a>
                <a href="<?php echo $previous ?>" title="<?php _e('Vorherige Seite', 'kalaflax'); ?>" class="posts__pagination-previous btn btn--icon">
                    <i class="icon-previous-page"></i>
                    <span class="screen-reader"><?php _e('Vorherige Seite', 'kalaflax'); ?></span>
                </a>
                <div class="posts__pagination-info">
                    <div class="posts__pagination-count">
                        <?php echo $posts; ?>
                    </div>
                    <div class="posts__pagination-current-page">
                        <?php echo $current; ?>
                    </div>
                </div>
                <a href="<?php echo $next ?>" title="<?php _e('Nächste Seite', 'kalaflax'); ?>" class="posts__pagination-next btn btn--icon">
                    <i class="icon-next-page"></i>
                    <span class="screen-reader"><?php _e('Nächste Seite', 'kalaflax'); ?></span>
                </a>
                <a href="<?php echo $last ?>" title="<?php _e('Letzte Seite', 'kalaflax'); ?>" class="posts__pagination-last btn btn--icon">
                    <i class="icon-last-page"></i>
                    <span class="screen-reader"><?php _e('Letzte Seite', 'kalaflax'); ?></span>
                </a>
            </nav>
        </div>
        <?php
    }

	add_action( 'wp_ajax_klflx_newsletter_signup', 'klflx_newsletter_signup' );
	add_action( 'wp_ajax_nopriv_klflx_newsletter_signup', 'klflx_newsletter_signup' );

	function klflx_get_newsletter_form_nonce() {
		return wp_create_nonce('klflx-newsletter-signup');
	}

	function klflx_check_newsletter_form_nonce($nonce) {
		return wp_verify_nonce($nonce, 'klflx-newsletter-signup');
	}

	function klflx_newsletter_signup() {

		header('Content-Type: application/json');

		// Check nonce
		if ( ! klflx_check_newsletter_form_nonce( $_POST['nonce'] ) ) {
			header($_SERVER['SERVER_PROTOCOL'] . ' 401 Unauthorized', true, 401);
			die( json_encode( array( 'error' => 'Ein Fehler ist aufgetreten. Bitte laden Sie die Seite erneut und probieren Sie es noch einmal.' ) ) );
		}

		// First name
		$firstname = sanitize_text_field( $_POST['firstname'] );
		if ( '' === $firstname ) {
			header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request', true, 400);
			die( json_encode( array( 'error' => 'Bitte geben Sie Ihren Vornamen ein', 'field' => 'firstname' ) ) );
		}

		// Last name
		$lastname = sanitize_text_field( $_POST['lastname'] );
		if ( '' === $lastname ) {
			header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request', true, 400);
			die( json_encode( array( 'error' => 'Bitte geben Sie Ihren Nachnamen ein', 'field' => 'lastname' ) ) );
		}

		// Email
		$email = sanitize_email( $_POST['email'] );
		if ( ! is_email( $email )) {
			header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request', true, 400);
			die( json_encode( array( 'error' => 'Bitte geben Sie eine gültige E-Mail Adresse ein', 'field' => 'email' ) ) );
		}

		// Company
		$company = sanitize_text_field( $_POST['company'] );

		$message = "Newsletter Anmeldung: \n\nName: $firstname $lastname\nE-Mail: $email\nFirma: $company";

		if(!wp_mail( 'k.schmitt@kalaflax.de', 'Newsletter Anmeldung', $message )) {
			error_log("Could not send newsletter subscription email.");
			header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
			die( json_encode( array( 'error' => 'Leider konnten wir Sie auf Grund eines Fehlers nicht zum Newsletter anmleden. Bitte versuchen Sie es später noch einmal.' ) ) );
        }

		die(json_encode( array( 'success' => 'Anmeldung erfolgreich. Willkommen beim Newsletter!' )));
	}

    function klflx_is_external_url($url)
    {
        $urlHost = parse_url($url, PHP_URL_HOST);
        if (empty($urlHost) || $urlHost === $_SERVER['SERVER_NAME']) {
            return false;
        }

        return true;
    }

    function klflx_add_target_blank_if_external($url)
    {
        if (klflx_is_external_url($url)) {
            echo 'target="_blank"';
        }
    }

	function bb_event_date($date) {
	    if(strpos($date, '0:00') !== false) {
	        return substr($date, 0, strlen($date) - 7);
        }
        return $date . ' Uhr';
    }

    function bb_doc_criteria($doctor, $key) {
        bb_model_criteria($doctor, $key, "doc__criteria-img");
    }

    function bb_rest_criteria($restaurant, $key) {
        bb_model_criteria($restaurant, $key, "restaurant__criteria-img");
    }

    function bb_model_criteria($model, $key, $cssClass) {
        $answer = 'unknown';
        $label = 'Unbekannt';
        if(array_key_exists($key, $model)) {
            switch($model[$key]) {
                case 'ok':
                    $answer = 'yes';
                    $label = 'Ja';
                    break;
                case 'cancel':
                    $label = 'Nein';
                    $answer = 'no';
                    break;
            }
        }

        ?><img src="<?php klflx_asset_img_url("icons/$answer.svg"); ?>" title="<?php echo $label ?>" class="<?php echo $cssClass; ?>"/><?php
    }

    function bb_external_href($content)
    {
        return preg_replace_callback('/<a[^>]+/', function ($matches) {
            $link = $matches[0];

            if (strpos($link, 'href') === false) {
                return $link;
            }

            if (strpos($link, 'target') !== false) {
                return $link;
            }

            return preg_replace_callback('/href=\S*"([^"]+)"/i', function ($hrefMatches) {

                $href = $hrefMatches[0];
                if (count($hrefMatches) < 2) {
                    return $href;
                }

                $url = $hrefMatches[1];
                if (!klflx_is_external_url($url)) {
                    return $href;
                }

                return $href . ' target="_blank"';
            }, $link);
        }, $content);
    }

    add_filter('the_content', 'bb_external_href');
    add_filter('the_excerpt', 'bb_external_href');
}
